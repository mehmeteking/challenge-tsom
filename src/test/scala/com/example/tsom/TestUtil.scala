package com.example.tsom

import java.util.Date
import scala.language.implicitConversions

object TestUtil extends org.specs2.mock.Mockito {

  val now = new Date
  val hour = 60 * 60 * 1000
  def date(i: Int) = new Date(now.getTime + i * hour)

  def mockEvent(performance: Performance) = {
    val event = mock[Event]
    event.start returns performance.start
    event.end returns performance.end
    event.performance returns performance
    event
  }

  def mockPerformance(start: Date, end: Date, priority: Int = 1, band: String = "") = {
    val perf = mock[Performance]
    perf.start returns start
    perf.end returns end
    perf.priority returns priority
    perf.band returns band
    perf
  }

  implicit def pairToTimed(pair: (Date, Date)): Timed = {
    val tmd = mock[Timed]
    tmd.start returns pair._1
    tmd.end returns pair._2
    tmd
  }

}
