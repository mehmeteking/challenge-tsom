package com.example.tsom

import scala.util.Random

class TestSchedule extends org.specs2.mutable.Specification {

  import TestUtil._

  "Schedule" should {

    def compare(t1: Timed, t2: Timed) = {
      "have same start" in (t1.start must beEqualTo(t2.start))
      "have same end" in (t1.end must beEqualTo(t2.end))
    }

    val schedule = new Schedule()
    val times = for (i <- 1 to 9) yield (date(i), date(i + 4))
    val pers = times.zipWithIndex.map(t => mockPerformance(t._1._1, t._1._2, 1, s"band${t._2}"))

    "add correctly" in {

      "when empty" in {
        val sch = schedule.add(pers(0))
        sch.events.size must beEqualTo(1)
        compare(sch.events.head, pers(0))
      }

      "when an event exists" in {
        val sch = schedule.add(pers(0)).add(pers(4))
        sch.events.size must beEqualTo(2)
        compare(sch.events.head, pers(0))
        compare(sch.events.tail.head, pers(4))
      }

      "when events intersect" in {

        "when lower bound" in {
          val sch = schedule.add(pers(0)).add(pers(8)).add(pers(1))
          sch.events.size must beEqualTo(3)
          compare(sch.events.head, pers(0))
          compare(sch.events.tail.head, (pers(0).end, pers(1).end))
          compare(sch.events.last, pers(8))
        }

        "when higher bound" in {
          val sch = schedule.add(pers(0)).add(pers(8)).add(pers(7))
          sch.events.size must beEqualTo(3)
          compare(sch.events.head, pers(0))
          compare(sch.events.tail.head, (pers(7).start, pers(8).start))
          compare(sch.events.last, pers(8))
        }

        "when upper and lower bound" in {
          val sch = schedule.add(pers(0)).add(pers(6)).add(pers(2))
          sch.events.size must beEqualTo(3)
          compare(sch.events.head, pers(0))
          compare(sch.events.tail.head, (pers(0).end, pers(6).start))
          compare(sch.events.last, pers(6))
        }
      }

      "when events fully overlap" in {
        val longEvent = mockPerformance(date(1), date(8))
        val sch = schedule.add(pers(2)).add(pers(8)).add(longEvent)
        sch.events.size must beEqualTo(4)
        compare(sch.events.head, (date(1), pers(2).start))
        compare(sch.events.tail.head, (pers(2).start, pers(2).end))
        compare(sch.events.tail.tail.head, (pers(2).end, date(8)))
        compare(sch.events.last, (pers(8).start, pers(8).end))
      }
    }
  }

  "sort correctly" in {
    val random = Random.shuffle(for (i <- 1 to 10) yield mockPerformance(now, now, i)).toArray
    val sorted = Schedule.sort(random).map(_.priority).toSeq
    sorted must beEqualTo (for (i <- 1 to 10) yield 11 - i)
  }
}
