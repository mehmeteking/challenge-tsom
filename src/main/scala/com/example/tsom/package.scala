package com.example

import java.util.Date

package object tsom {

  trait Timed {
    def start: Date
    def end: Date
  }
  object Timed

}
