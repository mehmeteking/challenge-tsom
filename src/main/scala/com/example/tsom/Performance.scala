package com.example.tsom

import java.util.Date

case class Performance(band: String, start: Date, end: Date, priority: Int) extends Timed
object Performance
