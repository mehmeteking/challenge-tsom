package com.example.tsom

import java.util.Date
import scala.collection.SortedSet
import scala.util.Sorting

class Schedule private (val events: SortedSet[Event]) {
  import Schedule._

  def this() = this(SortedSet.empty(Ordering[Long].on[Event](_.start.getTime)))

  def add(performance: Performance) = addEvent(performance.toEvent)
  def lastBefore(event: Event) = events.until(event).lastOption
  def firstAfter(event: Event) = events.from(event).headOption
  def addEvent(event: Event): Schedule = addBetween(event.performance, lastBefore(event), firstAfter(event))

  private[this] def addBetween(performance: Performance, before: Option[Event], after: Option[Event]) = {
    val begin = max(before.map(_.end).getOrElse(performance.start), performance.start)
    val end = min(after.map(_.start).getOrElse(performance.end), performance.end)

    val sch = if (begin.before(end)) new Schedule(events + Event(begin, end, performance)) else this

    if (after.isDefined && after.get.end.before(performance.end))
      sch.add(Performance(performance.band, after.get.end, performance.end, performance.priority))
    else sch
  }

}
object Schedule {

  private def min(d1: Date, d2: Date) = if (d1.before(d2)) d1 else d2
  private def max(d1: Date, d2: Date) = if (d1.after(d2)) d1 else d2

  // main method
  def apply(performances: Array[Performance]) = sort(performances).foldLeft(new Schedule())((s, p) => s.add(p))

  def sort(performances: Array[Performance]) = {
    Sorting.quickSort(performances)(Ordering[Int].on[Performance](- _.priority))
    performances
  }

  implicit class PerformanceWrapper(val p: Performance) extends AnyVal {
    def toEvent = Event(p.start, p.end, p)
  }

}

case class Event(start: Date, end: Date, performance: Performance) extends Timed
object Event
